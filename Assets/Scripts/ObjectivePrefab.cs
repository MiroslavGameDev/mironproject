﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class ObjectivePrefab : MonoBehaviour 
{
	[SerializeField]
	private Text ObjectiveName;
	[SerializeField]
	private Slider Slider;
	private EditPanel EditPanel;
	private GameObject AddProgressPanel;
	private GameObject NameObj;
	private Objective myObjective;
	private ProgressPanel ProgressPanel;

	public void OpenEditPanel()
	{
		EditPanel.Open (myObjective);
	}

	public void OpenProgressPanel ()
	{
		ProgressPanel.OpenProgressPanel (myObjective);	
	}

	public void Init(EditPanel editPanel, ProgressPanel progressPanel)
	{
		EditPanel = editPanel;
		ProgressPanel = progressPanel;
	}

	public void FillObjectiveInfo(Objective objective)
	{
		myObjective = objective;
		ObjectiveName.text = myObjective.Name;
		Slider.maxValue = myObjective.MaxProgress;
		Slider.value = myObjective.CurrentProgress;
	}
}
