﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;


public class MainPanel : MonoBehaviour
{
	[SerializeField]
	private ObjectivePrefab Prefab;
	[SerializeField]
	private EditPanel EditPanel;
	[SerializeField]
	private ProgressPanel ProgressPanel;
	[SerializeField]
	private CreateObjectivePanel CreateObjectivePnl;
	[SerializeField]
	private Transform Objectives;
	[SerializeField]
	private Text NameInfo; // TODO : Name info
//панель куда инстансить наши objective префабы
	private int quantityPrefabs;
	private List<ObjectivePrefab> myObjectivePrefabs = new List<ObjectivePrefab> ();

	private void Awake ()
	{
		EditPanel.Init (RefreshInfo);
		CreateObjectivePnl.Init (AddObjective);
		ProgressPanel.Init (RefreshInfo);
	}

	private void Start ()
	{
		string myPrefs = PlayerPrefs.GetString (Constants.ObjectivePrefsKey);
		SpamObjectivesPrefabs ();
	}

	public void RefreshInfo () // Обновляем инфу которую поменяли в прогресс или едит панеле
	{	
		ListObject myObj = GetCurrentObjectiveList () ;

		for (int i = 0; i < myObjectivePrefabs.Count; i++) 
		{
			myObjectivePrefabs [i].FillObjectiveInfo (myObj.Objectivelist [i]);
		}
	}

	public void AddObjective (Objective Obj)// Добавляем префаб для цели которую мы создали в рантайме
	{
		ObjectivePrefab objPrefab = Instantiate (Prefab, Objectives) as ObjectivePrefab;
		string name = Obj.Name;
		int curProgress = Obj.CurrentProgress;
		int maxProgress = Obj.MaxProgress;
		objPrefab.Init (EditPanel, ProgressPanel);
		objPrefab.FillObjectiveInfo (Obj);
		myObjectivePrefabs.Add (objPrefab);
	}

	private void SpamObjectivesPrefabs ()//Спамим префабы обджективов при старте приложения
	{
		ListObject myObj = GetCurrentObjectiveList ();

		if (myObj == null)
			return;

		for (int i = 0; i < myObj.Objectivelist.Count; i++) 
		{
			if (Login.InfoIdUser ==myObj.Objectivelist[i].UserId) 
			{
				AddObjective (myObj.Objectivelist [i]);

			}	

		}
	}

	public void Create ()
	{
		CreateObjectivePnl.Show ();
	}

	public static ListObject GetCurrentObjectiveList ()//использовать один и тот же метод каждый раз когда подтягиваем инфо с префсов
	{
		string savedString = PlayerPrefs.GetString (Constants.ObjectivePrefsKey);
		ListObject myObj = JsonUtility.FromJson<ListObject> (savedString);
		return myObj;
	}

	public static void SaveObjectListToPlayerPrefs (ListObject ListObj)
	{
		string jsonString = JsonUtility.ToJson (ListObj, true);
		PlayerPrefs.SetString (Constants.ObjectivePrefsKey, jsonString);	
	}
	public void Exit ()
	{
		Debug.Log ("Work Exit");
		SceneManager.LoadScene(0);
	}
}