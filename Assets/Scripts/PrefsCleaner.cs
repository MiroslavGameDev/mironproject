﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefsCleaner : MonoBehaviour
{
	private void Update ()
	{
		if(Input.GetKeyDown(KeyCode.C)) //c mean clear
		{
			ClearObjectivePrefs ();
		}
	}

	private void ClearObjectivePrefs()
	{
		string key = Constants.ObjectivePrefsKey;
		PlayerPrefs.DeleteKey (key);
		string Userkey = Constants.UserPrefsKey;
		PlayerPrefs.DeleteKey (Userkey);
		Debug.Log ("[PlayerPrefsCleaner] Prefs by " + key + " key cleared");
	}
}
