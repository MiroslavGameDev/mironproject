﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Menu : MonoBehaviour
{

	[SerializeField]
	private Registr RegistrPanel;
	[SerializeField]
	private Login LoginPanel;

	public void Login ()
	{
		LoginPanel.gameObject.SetActive (true);
		Debug.Log ("Work login ");
	}

	public void Registr ()
	{
		RegistrPanel.gameObject.SetActive (true);
		Debug.Log ("Work Registr");
	}

	public void Exit ()
	{
		Debug.Log ("Work Exit");
		Application.Quit ();
	}
	public  UserDataBase GetCurrentUserList ()
	{
		string savedString = PlayerPrefs.GetString (Constants.UserPrefsKey);
		UserDataBase myObj = JsonUtility.FromJson<UserDataBase> (savedString);
		return myObj;
	}
}
