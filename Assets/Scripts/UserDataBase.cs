﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
[Serializable]
public class UserDataBase  
{
	public List<UserInfo> UserList = new List<UserInfo> ();

}

[Serializable]
public class UserInfo
{
	public string Email;
	public string Name;
	public string Nick ;
	public string Pass;
	public int Id;
}