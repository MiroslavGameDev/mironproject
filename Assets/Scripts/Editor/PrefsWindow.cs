using UnityEngine;
using UnityEditor;

public class PrefsViewerWindow : EditorWindow
{
	private static PrefsViewerWindow _window;
	private Vector2 _scrollPos;

	[MenuItem ("Window/PrefsViewerWindow")]
	static void Init ()
	{
		_window = (PrefsViewerWindow)GetWindow (typeof(PrefsViewerWindow));
		_window.Show ();
	}

	void OnGUI ()
	{
		GUILayout.Label ("Objectives Prefs", EditorStyles.boldLabel);
		_scrollPos = EditorGUILayout.BeginScrollView (_scrollPos, true, true, GUILayout.Width (_window.position.width), GUILayout.Height (_window.position.height));
		string prefs = PlayerPrefs.GetString (Constants.ObjectivePrefsKey);
		GUILayout.Label (prefs);
		EditorGUILayout.EndScrollView ();
	}
	 

}