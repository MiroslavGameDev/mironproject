﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using System;

public class CreateObjectivePanel : MonoBehaviour
{
	[SerializeField]
	private InputField NameChallenge; 
	[SerializeField]
	private InputField Time;
	[SerializeField]
	private InputField Quantity;
	private Action<Objective> addObjective;

	public void Init (Action<Objective>  addObj) 
	{
		addObjective = addObj; 
	}

	public void Show ()
	{
		gameObject.SetActive (true);
	}

	public void Hide ()
	{
		gameObject.SetActive (false);
	}

	public void CreateObjective ()
	{
		ListObject myObjList = GetObjectList();
		Objective myObj = FillObjective (myObjList.Objectivelist.Count); 
		myObjList.Objectivelist.Add (myObj); 
		SaveToPlayerPrefs (myObjList); 
		Hide ();
		addObjective (myObj);
		NameChallenge.text = null;
		Time.text = null;
		Quantity.text = null;
	}

	private Objective FillObjective (int id)
	{
		Objective myObj = new Objective ();
		myObj.Name = NameChallenge.text;
		myObj.Time = Time.text;
		myObj.MaxProgress = System.Convert.ToInt32 (Quantity.text);
		myObj.Id = id;
		myObj.UserId = Login.InfoIdUser;
		return myObj;
	}

	private ListObject GetObjectList ()
	{
		ListObject mySavedList = MainPanel.GetCurrentObjectiveList(); 
		if (mySavedList != null)
			return mySavedList;
		else
			return new ListObject ();
	}
	 
	private void SaveToPlayerPrefs (ListObject myObjList) 
	{
		string myJsonString = JsonUtility.ToJson (myObjList, true); 
		PlayerPrefs.SetString (Constants.ObjectivePrefsKey, myJsonString); 
	}
		
}
	

