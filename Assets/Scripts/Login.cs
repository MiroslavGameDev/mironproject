﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class Login : MonoBehaviour
{
	public static int InfoIdUser;

	[SerializeField]
	private Registr RegistrPanel;
	[SerializeField]
	private Login LoginPanel;
	[SerializeField]
	private Menu MenuPanel;
	[SerializeField]
	private InputField MailField;
	[SerializeField]
	private InputField PassField;
	[SerializeField]
	private Text TextPlzReg;

	void Start ()
	{
		string print = PlayerPrefs.GetString (Constants.UserPrefsKey);
		Debug.Log (print);
	}

	private bool CheckUser ()
	{
		string UserInfo = PlayerPrefs.GetString (Constants.UserPrefsKey);
		UserDataBase myUser = JsonUtility.FromJson<UserDataBase> (UserInfo);
		for (int i = 0; i < myUser.UserList.Count; i++) 
		{
			if (MailField.text == myUser.UserList [i].Email && PassField.text == myUser.UserList [i].Pass) 
			{
				InfoIdUser = i;
				return true;
			} 
		
		}
		return false;

	}

	public void LoginButton ()
	{
		if (CheckUser () == true) {
			SceneManager.LoadScene (1);
			Debug.Log ("Logining User Id " + InfoIdUser);
		} else {
			StartCoroutine (SetAvtiveObj ());
		}
	}

	public void MainMenuBtn ()
	{
		gameObject.SetActive (false);
	}

	IEnumerator SetAvtiveObj ()
	{
		TextPlzReg.text = "Pls Reg";
		TextPlzReg.gameObject.SetActive (true);
		yield return new WaitForSeconds (3);
		TextPlzReg.gameObject.SetActive (false);
	}
}
