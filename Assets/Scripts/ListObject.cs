﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public  class   ListObject
{
	public List<Objective> Objectivelist = new List<Objective> ();
}

[System.Serializable]	
public class Objective
{
	public int Id;
	public int UserId;
	public bool IsDone;
	public string Name;
	public string Time;
	public int MaxProgress;
	public int CurrentProgress;
	public List<ProgressStage> StageList;

	public override string ToString ()
	{
		return "IsDone = " + IsDone +
		"MaxProgress = " + MaxProgress +
		"Name " + Name +
		"Time " + Time +
		"CurrentProgress " + CurrentProgress;
	}
}

[System.Serializable]	
public class ProgressStage
{
	public string Date;
	public int Progress;

	public override string ToString ()
	{
		return "Date =" + Date + "Progress =" + Progress;
	}
}

