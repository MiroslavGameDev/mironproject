﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using System.Text.RegularExpressions;

public class ProgressPanel : MonoBehaviour
{
	[SerializeField]
	private InputField QuantityField;
	private Action refreshProgressInfo;
	private int idObj;

	public void Init (Action refresh)
	{
		refreshProgressInfo = refresh;
	}

	public void OpenProgressPanel (Objective objective)
	{ 
		Show ();
		idObj = objective.Id;
		QuantityField.text = objective.CurrentProgress.ToString ();
	}

	public void SaveProgress ()
	{
		ListObject myObjList = MainPanel.GetCurrentObjectiveList ();
		Objective myObj = myObjList.Objectivelist [idObj];
		myObj.CurrentProgress = myObj.CurrentProgress + System.Convert.ToInt32 (QuantityField.text); //TODO: validate value from user input ...can not be int (like "faggot" or smth like that)
		MainPanel.SaveObjectListToPlayerPrefs( myObjList);
		refreshProgressInfo ();
		Hide ();
	}

	private void Show()
	{
		gameObject.SetActive (true);
	}

	private void Hide()
	{
		gameObject.SetActive (false);
	}

	public void Del ()
	{
		PlayerPrefs.DeleteKey (Constants.ObjectivePrefsKey);
	}

	public void Quit ()
	{
		gameObject.SetActive (false);
	}
}
