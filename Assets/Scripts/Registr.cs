﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System;
using UnityEngine;
using UnityEngine.UI;


public class Registr : MonoBehaviour
{
	[SerializeField]
	private InputField Mail_field;
	[SerializeField]
	private InputField Pass_field;
	[SerializeField]
	private InputField ConfirmPass_field;
	[SerializeField]
	private InputField Name_field;
	[SerializeField]
	private InputField Nick_field;
	[SerializeField]
	private Text TextPassDontMatch;
	[SerializeField]
	private Text MailIsNotValid;
	[SerializeField]
	private Menu Menu ;

	private Regex email = new Regex (@"[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");

	private bool CheckPassMatch ()
	{
		return Pass_field.text == ConfirmPass_field.text;
	}

	private bool CheckMail ()
	{
		return email.IsMatch (Mail_field.text);
	}

	private void RefreshInputField ()
	{
		Mail_field.text = null;
		Pass_field.text = null;
		ConfirmPass_field.text = null;
		Name_field.text = null;
		Nick_field.text = null;
	}

	public void MainMenuBtn ()
	{
		gameObject.SetActive (false);
	}

	IEnumerator SetAvtiveTextPass ()
	{
		TextPassDontMatch.text = "Passwords don't match";
		TextPassDontMatch.gameObject.SetActive (true);
		yield return new WaitForSeconds (3);
		TextPassDontMatch.gameObject.SetActive (false);
	}

	IEnumerator SetAvtiveTextMail ()
	{
		MailIsNotValid.text = "Mail is not valid";
		MailIsNotValid.gameObject.SetActive (true);
		yield return new WaitForSeconds (3);
		MailIsNotValid.gameObject.SetActive (false);
	}

	public void RegButton ()
	{
		if (CheckMail () == false) {

			StartCoroutine (SetAvtiveTextMail ());
			return;
		}

		if (CheckPassMatch () == false) {
			StartCoroutine (SetAvtiveTextPass ());
			return;
		}
		gameObject.SetActive (false);
		AddUser ();
		RefreshInputField ();
	}

	private void AddUser ()
	{
		UserDataBase myUserList = GetUserList ();
		UserInfo myInfo = FillUserInfo (myUserList.UserList.Count);
		myUserList.UserList.Add (myInfo);
		string myJsonString = JsonUtility.ToJson (myUserList);
		PlayerPrefs.SetString (Constants.UserPrefsKey, myJsonString);
	}

	private UserDataBase GetUserList ()
	{
		UserDataBase mySavedList = Menu.GetCurrentUserList ();
		if (mySavedList != null)
			return mySavedList;
		else
			return new UserDataBase ();
	}

	private UserInfo FillUserInfo (int id)
	{
		UserInfo myInfo = new UserInfo ();
		myInfo.Name = Name_field.text;
		myInfo.Nick = Nick_field.text;
		myInfo.Email = Mail_field.text;
		myInfo.Pass = Pass_field.text;
		myInfo.Id = id;
		return myInfo;
	}

}
