﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Text.RegularExpressions;

public class EditPanel : MonoBehaviour
{
	[SerializeField]
	private InputField NameChallenge;
	[SerializeField]
	private InputField TimeImputField;
	[SerializeField]
	private InputField QuantityInputField;
	private Action refreshInfo;
	private int objId;

	public void Init (Action refresh)
	{
		refreshInfo = refresh;
	}

	public void Open (Objective objective)
	{
		gameObject.SetActive (true);
		objId = objective.Id;
		NameChallenge.text = objective.Name;
		TimeImputField.text = objective.Time;
		QuantityInputField.text = objective.MaxProgress.ToString (); 
	}

	public void EditToPlayerPrefs ()
	{
		ListObject myObjList = MainPanel.GetCurrentObjectiveList ();
		Objective myObj = myObjList.Objectivelist [objId];
		myObj.Name = NameChallenge.text;
		myObj.Time = TimeImputField.text;
		myObj.MaxProgress = System.Convert.ToInt32 (QuantityInputField.text);//TODO: validate value from user input ...can not be int (like "faggot" or smth like that)
		MainPanel.SaveObjectListToPlayerPrefs( myObjList);
		refreshInfo ();
		gameObject.SetActive (false);
	}
}
